var debug = require('debug')('annulet:signup'),
  counter = require('../../counter'),
  path = require('path');

module.exports = exports = function() {
  return function(nightmare) {
  	debug('ownerinfo');
  	nightmare.click('a[href="/signup/ownerInformation"]')
      .wait(2000)
      .screenshot(counter.makePath('signup','ownerIfno.png'))
      .wait(500)
      .type('#firstName', 'Susan')
      .type('#lastName', 'Owner')
      .type('#email', 'rosshinkley+annulet@gmail.com')
      .type('#password', 'test')
      .type('#passwordAgain', 'test');
  };
};
