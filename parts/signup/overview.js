var debug = require('debug')('annulet:signup'),
  counter = require('../../counter'),
  path = require('path');

module.exports = exports = function() {
  return function(nightmare) {
  	debug('overview');
    nightmare.goto('http://site.annulet.io/signup')
      //.wait('a[href="/termsOfUse"]')
      .wait(2000)
      .screenshot(counter.makePath('signup','overview.png'))
      .wait(500);
  };
};
