var debug = require('debug')('annulet:signup'),
  counter = require('../../counter'),
  path = require('path');

module.exports = exports = function() {
  return function(nightmare) {
  	debug('company info');
  	nightmare.click('a[href="/signup/companyInformation"]')
      .wait(2000)
      .screenshot(counter.makePath('signup','companyInfo.png'))
      .wait(500)
      .type('#companyName', 'Susan User Solutions')
      .type('#address1', '123 Main St.')
      .type('#address2', 'Suite 16')
      .type('#city', 'New Town')
      .type('#state', 'IL')
      .type('#zip', '60606')
      .type('#phone', '(555)654-3210')
      .type('#numberOfLocations', '6')
      .type('#numberOfEmployees', '10');
  };
};
