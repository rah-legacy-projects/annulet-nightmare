module.exports = exports = {
	"companyInformation": require("./companyInformation"),
	"overview": require("./overview"),
	"ownerInformation": require("./ownerInformation"),
	"payment": require("./payment"),
	"privacy": require("./privacy"),
	"termsOfUse": require("./termsOfUse"),
	"warrantyDisclaimer": require("./warrantyDisclaimer"),
};
