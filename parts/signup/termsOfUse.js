var debug = require('debug')('annulet:signup'),
  counter = require('../../counter'),
  path = require('path');

module.exports = exports = function() {
  return function(nightmare) {
  	debug('terms of use');
    nightmare.click('a[href="/signup/termsOfUse"]')
	  .wait(2000)
      .screenshot(counter.makePath('signup', 'termsOfUse.png'))
      .wait(500);
  };
};
