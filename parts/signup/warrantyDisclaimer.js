var debug = require('debug')('annulet:signup'),
  counter = require('../../counter'),
  path = require('path');

module.exports = exports = function() {
  return function(nightmare) {
  	debug('warranty disclaimer');
    nightmare.click('a[href="/signup/disclaimer"]')
      .wait(3000)
      .screenshot(counter.makePath('signup', 'warrantyDisclaimer.png'))
      .wait(500);
  };
};
