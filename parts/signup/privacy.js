var debug = require('debug')('annulet:signup'),
  counter = require('../../counter'),
  path = require('path');

module.exports = exports = function() {
  return function(nightmare) {
  	debug('privacy');
    nightmare.goto('http://site.annulet.io/signup/privacy')
      .wait(4000)
      .screenshot(counter.makePath('signup', 'privacy.png'))
      .wait(500);
  };
};
