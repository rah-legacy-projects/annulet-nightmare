var debug = require('debug')('annulet:signup'),
  counter = require('../../counter'),
  path = require('path');

module.exports = exports = function() {
  return function(nightmare) {
    debug('terms of use');
    nightmare.click('a[href="/signup/payment"]')
      .wait(2000)
      .screenshot(counter.makePath('signup', 'payment.png'))
      .wait(1000)
      .type('#creditCardNumber','4242424242424242')
      .type('#cvc', '424')
      .type('#expmonth', 'd')
      .type('#expyear', '222')
      .wait(500)
      .screenshot(counter.makePath('signup', 'payment-filled.png'));
      .click('#finish')
      .wait(60000)
      .screenshot(counter.makePath('signup', 'finish.png'));
  };
};
