var path = require('path'),
  mkdirp = require('mkdirp');
var ex = {
  dirNumber: 0,
  shotNumber: 0,

};

ex.makePath = function(dir, pic) {
  var dirpad = '000'.substring(0, 3-(ex.dirNumber.toString()).length)  + ex.dirNumber.toString();
  var shotpad = '000'.substring(0, 3-(ex.shotNumber.toString()).length)  + ex.shotNumber.toString();
  var picpath = path.resolve(__dirname, 'shots', dirpad+'_'+dir, shotpad+'_'+pic);
  mkdirp.sync(path.dirname(picpath));
  ex.shotNumber++;
  return picpath;
};

module.exports = exports = ex;
