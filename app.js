var Nightmare = require('nightmare'),
    debug=require('debug')('annulet'),
    nightmare = Nightmare(),
    counter = require('./counter'),
    parts = require('./parts');

nightmare
    .viewport(1280, 1024)
    .use(parts.signup.overview())
    .use(parts.signup.termsOfUse())
    .use(parts.signup.warrantyDisclaimer())
    .use(parts.signup.privacy())
    .use(parts.signup.ownerInformation())
    .use(parts.signup.companyInformation())
    .use(parts.signup.payment())
    .then(function(){
        debug('done');
        nightmare.end(function(){});
    })
    .catch(function(err){
        console.log('problems afoot');
        console.error(err);
    });
